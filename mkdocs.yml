site_name: openCFS User Documentation
site_dir: public
theme:
    name: material
    features:
        - navigation.tabs
        - navigation.tabs.sticky
        - navigation.sections
        - toc.integrate
    favicon: art/favicon.png
    icon:
        repo: fontawesome/brands/gitlab
site_url: https://opencfs.gitlab.io/userdocu/
repo_url: https://gitlab.com/openCFS/userdocu
repo_name: openCFS/CFS
markdown_extensions: 
    - pymdownx.arithmatex
    - attr_list
extra_javascript:
    - mathjax-config.js
    - https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML


plugins:
    - search
    - bibtex:
        bib_file: 'refs.bib'
        cite_style: 'pandoc'
#This is a comment
nav: #Naviation section. Insert here new sections/subsections.
    - openCFS: 'index.md' 
    - History: 'history.md'
    - Installation: 
        - Overview: 'Installation/README.md'
        - Pre-Processors: 'Installation/PrePorcessors.md'
        - XML Editors: 'Installation/XML-Editors.md'
        - openCFS: 'Installation/CFS.md'
        - ParaView Plugin: 'Installation/ParaView.md'
    - PDE Explanations:
        - Singlefield:
            - AcousticPDE: 'PDEExplanations/Singlefield/AcousticPDE/README.md'
            - ElectrostaticPDE: 'PDEExplanations/Singlefield/ElectrostaticPDE/README.md'
            - ElectricFlowPDE: 'PDEExplanations/Singlefield/ElectricFlowPDE/README.md'
            - HeatConductionPDE: 'PDEExplanations/Singlefield/HeatPDE/README.md'
            - MagneticPDE: 'PDEExplanations/Singlefield/MagneticPDE/README.md'
            - MechanicPDE: 'PDEExplanations/Singlefield/MechanicPDE/README.md'
            - LinFlowPDE: 'PDEExplanations/Singlefield/LinFlowPDE/README.md'
        - Coupledfield:
            - ElecFlowHeatPDE: 'PDEExplanations/Coupledfield/ElecThermPDE/README.md'
            - HeatMechPDE: 'PDEExplanations/Coupledfield/HeatMechPDE/README.md'
            - MechAcouPDE: 'PDEExplanations/Coupledfield/MechAcouPDE/README.md'
            - PiezoelectricPDE: 'PDEExplanations/Coupledfield/PiezoelectricPDE/README.md'
            - LinFlowHeatPDE: 'PDEExplanations/Coupledfield/LinFlowHeatPDE/README.md'
            - LinFlowAcouPDE: 'PDEExplanations/Coupledfield/LinFlowAcouPDE/README.md'
            - LinFlowMechPDE: 'PDEExplanations/Coupledfield/LinFlowMechPDE/README.md'
        - Optimization / Inverse Problems:
            - InverseSourceLocalization: 'PDEExplanations/Optimization_InverseProblems/InverseSourceLocalization/README.md'
    - Field Data Pre/Postprocessing:
        - General Information: 'DataExplanations/GeneralInformation/README.md'
        - Data Input/Output: 'DataExplanations/DataInputOutput/README.md'
        - Field Interpolators:
            - Conservative Interpolators: 'DataExplanations/ConservativeInterpolators/README.md'
            - Radial Basis Functions: 'DataExplanations/RBF/README.md'       
            - FE Based: 'DataExplanations/FEBased/README.md'   
            - Nearest Neighbour: 'DataExplanations/NN/README.md'        
            - Cell2Node and Node2Cell: 'DataExplanations/N2CC2N/README.md'
        - Aeroaocustic Source Terms:
            - Lamb Vector: 'DataExplanations/LambVector/README.md'
            - Lighthill Source Term: 'DataExplanations/LighthillSourceTerm/README.md'
            - Lighthill Source Term Vector: 'DataExplanations/LighthillSourceTermVector/README.md'
            - Time Derivative: 'DataExplanations/TimeDerivative/README.md'
        - Synthetic Sources:
            - SNGR: 'DataExplanations/SNGR/README.md'
        - Data Processing:
            - Curl: 'DataExplanations/Curl/README.md'
            - Divergence: 'DataExplanations/Divergence/README.md'
            - Gradient: 'DataExplanations/Gradient/README.md'
            - TimeMean: 'DataExplanations/TimeMean/README.md'
            - FIR: 'DataExplanations/FIR/README.md'
            - Temporal Blending: 'DataExplanations/TempBlend/README.md'
            - Volume Multiplication: 'DataExplanations/VolMulti/README.md'
        - FAQ: 'DataExplanations/faq/README.md'                           
    - Tutorials:
        - AnalysisWorkflow: 'Tutorials/AnalysisWorkflow/README.md'
        - Analysis-types: 'Tutorials/AnalysisTypesMechanicsCantileverBeam/README.md'
        - Thermal-Mechanic Coupling: 'Tutorials/HeatMechCoupling_CantileverBeam/README.md'
        - Piezoelectric Unit-Cube: 'Tutorials/Piezo_UnitCube/README.md'
        - Electrostatic Capacitor: 'Tutorials/Electrostatics_Capacitor/README.md'
        - Acoustics Cylindrical Wave: 'Tutorials/Acoustics_CylindricalWave2D/README.md'
        - Mechanics-Acoustics Coupled Eigenvalue Problem: 'Tutorials/MechAcou_CoupledEigenvalues/README.md'
        - System Matrix Export: 'Tutorials/SystemMatrixExport/SystemMatrix.md'
        - XML Explanations:
            - XML Editing: 'Tutorials/XMLExplanations/editing.md'
            - XML Structure: 'Tutorials/XMLExplanations/structure.md'
            - Result Explanation: 'Tutorials/XMLExplanations/results.md'
        - ParaView:
          - Overview: 'Tutorials/ParaView/README.md'
          - Common Tasks: 'Tutorials/ParaView/basics.md'
          - Animate Harmonic Results: 'Tutorials/ParaView/AnimateHarmonicResults/README.md'
        - Features:
            - Non-Conforming Interfaces: 'Tutorials/Features/ncinterfaces.md'
            - Rotating Interfaces: 'Tutorials/Features/rotatingncinterfaces.md'
            - Perfectly Matched Layer: 'Tutorials/Features/pml.md'
            - Absorbing Boundary Condition: 'Tutorials/Features/abc.md'
            - Math Expressions: 'Tutorials/Features/mathexpressions.md'
        - Meshing:
            - General information: 'Tutorials/Meshing/Trelis.md'
            - Using COMSOL as mesh-generator: 'Tutorials/Meshing/comsol.md'
        - Postprocessing with Python: 
            - Postprocessing with Python: 'Tutorials/PythonPostProcessing/README.md'
    - Applications:
        - SingleField:
            - Acoustics:
                - Sound-Barrier: 'Applications/Singlefield/Acoustics/SoundBarrier/README.md'
                - MPP Muffler: 'Applications/Singlefield/Acoustics/MPP_Muffler/README.md'                
            - Mechanics: 
                - Cantilever: 'Applications/Singlefield/Mechanics/Cantilever/README.md'
        - CoupledField:
            - HeatMech:
                - Heat sink: 'Applications/Coupledfield/HeatMech/HeatSink/README.md'      
            - Aeroacoustics:                     
                - Cylinder in a Crossflow: 'Applications/Singlefield/Acoustics/CylinderFlow/README.md'                
                - Human Phonation: 'Applications/Singlefield/Acoustics/HumanPhonation/README.md'
    - Exercises: 
        - Multiphysics 2:
            - General Information: 'Exercises/Multiphysics2/README.md'
            - HW 1 Linear Magnetic: 'Exercises/Multiphysics2/1_Magnetics_Linear/README.md'
            - HW 2 Non-linear Magnetic: 'Exercises/Multiphysics2/2_Magnetics_Nonlin/README.md'
            - HW 3 Magnetic Mechanic: 'Exercises/Multiphysics2/3_MagMech/README.md'
            - HW 4 Transient Coil: 'Exercises/Multiphysics2/4_EddyCurrents/README.md'
            - HW 5 Induction Heating: 'Exercises/Multiphysics2/5_MagHeat/README.md'
            - HW 6 Aeroacoustics: 'Exercises/Multiphysics2/6_CAA/README.md'
    - How to Contribute: 
        - Overview: 'Contributing/overview.md'
        - Prerequisits: 'Contributing/prerequisits.md'
        - Workflow:
            - Clone the repository:  'Contributing/clone.md'
            - Edit Userdocumentation: 'Contributing/edit.md'
            - Create Merge Request: 'Contributing/merge.md'
