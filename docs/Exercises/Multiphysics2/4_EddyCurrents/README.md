# HW4: Transient Coil


### Description
Consider an axi-symmetric model of a coil and a plate as depicted in the figure.
The inner and outer radii of the coil, $r_1$ and $r_2$ , are 5 mm and 10 mm, respectively.
The length of the coil, $l_1$ , is 8 mm.
The plate thickness, $l_2$ , is 3 mm.
The point $C$ is located in the center of the coil, the point $P$ in the middle of the plate at $r = (r_1 + r_2)/2$. 

The plate is either made from iron or aluminum, with electric conductivity and linear magnetic permeability given in the table below. The coil has 40 turns and is fed by a controlled current.

![sketch](sketch.png){: style="width:200px"}

|                     | iron      | alu       |
| ------------------- | --------- | --------- |
| conductivity in S/m | 5.000e+06 | 3.500e+07 |
| permeability H/m    | 2.704e-03 | 1.257e-06 |

---------------------------

### Tasks
1. Create a regular hexaedral mesh using Trelis. **(1 Points)**
2. Create input-xml files for the required CFS++ simulations. **(1 Points)**
3. Document your results by answering the questions below. **(13 Points)**

### Hints
- Use the provided template [`coil.xml`](coil.xml).
- Adapt the Trelis input [`geometry-ue.jou`](geometry-ue.jou).
- Use the static simulation to check the prescribed current and boundary conditions. 
- Use a time resolution of 20 steps per period.
- You can use an element result or `sensorArray` to obtain results at the points $C$ and $P$.

### Submission
Submit all your input files (`*.jou`, `*.xml`) as well as a concise PDF report of your results. Name the archive according to the format `HWn_eNNNNNNN_Lastname.zip`.

---------------------------

### Mesh Size
Estimate the skin depth for both materials. What element size do you suggest? Motivate your suggestion! **(2 Points)**

### Static Analysis
Compute the static magnetic field for a coil current of 10 A. Plot the vector field of the magnetic flux density and the prescribed current density in the coil (in one figure). Compare the cases for iron and copper plate. What is the difference? **(3 Points)**

### Transient Analysis 
Set up a transient analysis with a sinusoidal coil current with an amplitude of 10 A and a frequency of 50 Hz. Compute the solution for 4 periods of the coil current. Plot the time signal of the radial flux component $B_{\rm r}$ at the point $P$ and the axial flux component $B_{\rm z}$ at the point $C$. Describe the time signals: Are there transient effects? Do the amplitudes correspond with the static ones? **(4 Points)**

### Skin Depth
Show the total current density as well as the vector field of the magnetic flux density for the time $t = 2.5/ f$ (in one plot for each plate material). How do the eddy currents decay over the thickness of the plate? What is the difference between the plate materials? **(4 Points)**

---------------------------

You can download the templates linked on this page individually by _right click --> save target as_.
Alternativey, head over to the [git repository](https://gitlab.com/openCFS/userdocu/-/tree/master/docs/Exercises/Multiphysics2) and use the _download button_ on the top right: 
Here is a direct link to the [__zip archive__](https://gitlab.com/openCFS/userdocu/-/archive/master/HW4.zip?path=docs/Exercises/Multiphysics2/4_MagMech).
