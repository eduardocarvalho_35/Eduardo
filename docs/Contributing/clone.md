# Obtain the repository
Please check if you have all [prerequisits](prerequisits.md).

If you dont have directly access to the project (which is most likley) you have to fork the projekt.
This means you simply mirroring the project into your namespace, and therefore gaining all rights on your namespace.
## How to fork
Go the the [userdocumentation repository](https://gitlab.com/openCFS/userdocu) and click the [`fork`-button](https://gitlab.com/openCFS/userdocu/-/forks/new) and follow the instructions. On the forked repository you have now all rights and you are able to edit everything. **Do not worry about deleting/destroying something**, if something stopped working you can simply reset everything, either by:

* deleting your forked repository and do all steps (forking & cloning) again. 
* or by using git: The commando `# git restore .` restores everything since your last commit.

## Clone the repository

Go to the (forked) userdocumentation repository on [gitlab.com](https://gitlab.com). It should be listed under _Projects => Your projects_.  Click onto the `Clone`-button and copy the `<ssh-link>`/`<https-link>`. The links should look like this:

* `<ssh-link>`: "git@gitlab.com:YOURNAMESPACE/userdocu.git" 
* `<https-link>`: "https://gitlab.com/YOURNAMESPACE/userdocu.git"

* **Under Ubuntu:** 
    - Open the terminal and go to the directory where the repository should be (use `# cd path/to/folder`)
    - clone the repostiory with this command: `# git clone <ssh-link or https-link>`
    
* **Under Windows:** 
    - Open the git bash and go to the directory where the repository should be (use `G# cd path/to/folder`)
    - clone the repository with this command: `G# git clone  <ssh-link or https-link>`
    
Now the repository should be locally on your computer.

With **git** you can now manage your repository. Some here are some useful commands:

* `git add .`: Adds new files and changes for your next commit.
* `git add filename`: Add a specific file to your next commit.
* `git commit`: Commiting all changes (which are added with `git add`) to your repository.
* `git checkout -b NewBranche`: Creates a new Branche called `NewBranche`
* `git checkout BrancheName`: Switches to the Branche called `BrancheName`. By default the master-branche is called `master`.
* `git pull`: Updates your local repostiory with the origin repository (the one on gitlab).
* `git push`: Pushes your local repository onto the original repository (the one on gitlab).

For a more detailed explaination please have a look into these [slides](slides_git.pdf) and dont hesitate to use google ;) .
