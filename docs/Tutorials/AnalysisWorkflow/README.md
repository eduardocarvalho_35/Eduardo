# Analysis Workflow

This tutorial illustrates the typical analysis workflow on the example of heat conduction in 2D.

All the files in this tutorial can be [`downloaded here`](AnalysisWorkflow.zip).
## Problem Definition


We consider stationary heat conduction through a wall in 2D.
Temperatures are given for the left and right boundary.
No heat flux through the top and bottom boundary.

What is the heat flux through the wall?


```
Sketch of the domain

  ___________
 |           |    ^
 |           |    |
 |   wall    |    b
 |           |    |
 |___________|    v

 <---- a ---->
  
```

![Sketch of the domain](Sketch-heat.png)


## Boundary Conditions

We set Dirichlet BCs at the left and right side of the domain directly defining the temperature.
Since nothing is set on the top and bottom boundary, the natural BC is used, whih physically enforces zero normal heat flux.


## Meshing


We use the *Trelis* GUI to create the domain:

1. Start it by typing `trelis` on the terminal.
2. Create a rectange
3. Mesh it
4. Assign and rename a *Block* for the *wall* domain
5. Use *Nodeset* for the boundaries
6. Export the mesh as a ANSYS-cdb file

The file [`wall.cdb`](wall.cdb) was created this way.

## Simulation with CFS

Use an XML-editor (e.g. *oXygen* or *eclipse*) to define the simulation input for CFS.

The input file [`simulation-input.xml`](simulation-input.xml) is the simulation input.
In the file [`mat.xml`](mat.xml) the material porperties are defined.
Both files are complete, and can be used for the example problem.

To start the computation run the following command in the terminal
```shell
cfs -p simulation-input.xml job
```
where `job` can be any name you choose for the simulation.

CFS will write some output on the terminal, and produce the two files
* `job.info.xml`, which contains some details about the run, and
* `job.cfs` in the `results_hdf5` directory, which you can view with ParaView.


## Postprocessing with ParaView


Open ParaView (by typing `paraview` in a terminal) and load the result file using File, Open... then click Apply.
