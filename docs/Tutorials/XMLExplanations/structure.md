# XML Input Scheme Explanations
This is just a summary of the most important and common xml commands and structure, for a more detailled explanation, visit the PDE specific descriptions, look at the provided examples or use the documentation of the xml scheme itself, e.g. via oxygen or eclipse

## XML Schema

The structure of the openCFS input (and material file) is defined in an XML scheme.
It is available direclty in the source code at [`share/xml`](https://gitlab.com/openCFS/cfs/-/blob/master/share/xml) as [`CFS-Simulation/CFS.xsd`](https://gitlab.com/openCFS/cfs/-/raw/master/share/xml/CFS-Simulation/CFS.xsd) for the simulation, and [`CFS-Material/CFS_Material.xsd`](https://gitlab.com/openCFS/cfs/-/raw/master/share/xml/CFS-Material/CFS_Material.xsd) for the material data base, respectively.

In order to use the auto-complete and validation features of XML editors they need to know the scheme.
This is acomplished by adding the attribute `xsi:schemaLocation="http://www.cfs++.org/simulation  ...some location..."` to the header tag `<cfsSimulation ...>`.
Note that defining the attriute is *optional* for cfs simulation runs.

The location can be:

* any local file path: as `file:/path/to/CFS-Simulation/CFS.xsd`
* an online location: as `http://...` or `https://...`

You can find the XML schema in several locations:

* installation directoy fo your your openCFS installation under `<install-directory>/share/xml/`
* source code repository at [gitlab.com/openCFS/cfs under `share/xml`](https://gitlab.com/openCFS/cfs/-/blob/master/share/xml) (select branch/tag you need)
* online at [https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd](https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd) for the simulation sheme, and equivalently for the material scheme (*latest version form master branch*).

e.g. to use the latest XML scheme from the master branch use
```
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd">
```

> The XML scheme is subject to changes, e.g. for new features.
> Make sure the XML scheme you use to create your input corresponds the the cersion od CFS you are running in order to avoid errors. 


## Skeleton of an openCFS xml input file
The underlying structure of our xml files is defined via a so-called xml-scheme.
The path to this scheme is defined in the header of every xml file and per default it points to our default public scheme, provided on our gitlab server:
```
<?xml version="1.0"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
```

In general, an OpenCFS xml file consists of three main parts:

* fileFormats: definition of input-/output- and material-files
* domain: region definitions (assign material to different regions)
* sequenceStep: contains the definition of the analysis type and the PDE. Several sequenceSteps can be concatenated and results from previous sequenceSteps can be processed in order to produce the desired result.


```
<!-- define which files are needed for simulation input & output-->
<fileFormats>
    <input>
        <!-- read mesh -->
    </input>
    <output>
        <!-- define output -->
    </output>
    <materialData file="../material/mat.xml" format="xml"/>
</fileFormats>

<domain geometryType="3d">
    <regionList>
        <!-- region definition -->
    </regionList>
</domain>

<sequenceStep index="1">
    <analysis>
        <!-- analysis type: static, transient, harmonic, multiharmonic, eigenfrequency -->
    </analysis>
    <pdeList>
        <!--for example consider the electric conduction PDE-->
        <elecConduction>
            <regionList>
                <!--define on which regions the PDE is solved-->
            </regionList>
            <bcsAndLoads>
                <!--define appropriate BC's-->
            </bcsAndLoads>
            <storeResults>
                <!--define the results to be stored-->
            </storeResults>
        </elecConduction>
    </pdeList>
</sequenceStep>
</cfsSimulation>
```


## Detailed Description
In the following the most important, PDE-independent, tags are described.
For a more detailled explanation, visit the PDE specific descriptions, look at the provided examples or use the documentation of the xml scheme itself, e.g. via oxygen or eclipse


### `fileFormats` Section

* `<input>` (mandatory) defines which files are read as input (either data and/or mesh)
* Different inputs are possible `gmsh`, `cdb`, `ensight`, `hdf5`. Several more are available in the xml scheme but they are not maintained
* When reading more than one file, unique `id's` have to be provided
* `scaleFac=` (optional) scales the provided mesh with a prescribed factor, e.g. conversion from mm to m
* `<scatteredData>` (optional) can read some pointwise data, for example from a csv file
* `<output>` (mandatory) defines the output format, most of the time you want to export the results in h5 format, sometimes also in text format, e.g. global integrated quantities
* `<materialData>` (mandatory) defines the location of the material file

```
<fileFormats>
    <input>
      <gmsh fileName="../meshes/model_indwat.msh2" id="i1" scaleFac="1e-3"/>
      <gmsh fileName="../meshes/model_air.msh2" id="i2"/>
      <gmsh fileName="../meshes/model_sheet.msh2" id="i4"/>
    </input>
    <scatteredData> 
        <csv fileName="testinput.csv" id="inCSV">
            <coordinates>
                <comp dof="x" col="1"/>
                <comp dof="y" col="2"/>
                <comp dof="z" col="3"/>
            </coordinates>
            <quantity name="heatSourceDensity" id="hsd1">
                <comp dof="x" col="0"/>
            </quantity>
        </csv>
    </scatteredData>
   <output>
      <hdf5/>
      <text id="txt"/>
   </output>
   <materialData file="../material/mat.xml" format="xml"/>
</fileFormats>
```


### `domain` Section

* `<regionList>` (mandatory) define the material of every region in the mesh file(s)

```
  <domain geometryType="plane">
    <regionList>
      <region material="air" name="region_1"/>
      <region material="iron" name="region_2"/>
      <region material="air" name="region_3"/>
    </regionList>
  </domain>
```

### `sequenceStep` Section

* `<analysis>` (mandatory) define the kind of analysis to be performed (static, harmonic, transient…).
* Only one analysis type can be done per sequence step.
* `<pdeList>` (mandatory) contains all pde's used in this sequence step. There can be multiple pde's in one sequence step e.g. if you use direct coupling.
    * `<pdeName>` (mandatory) defines one PDE and all needed information
        * `<regionList>` (mandatory) define the regions where that pde is defined and computed on
        * `<bcsAndLoads>` (mandatory) define the boundary conditions (dirichlet BC, neumann BC, ...) and loads (heat source density, ...) of this pde.
        * `<storeResults>` (mandatory) define the results that we want to get from the analysis (e.g. Temperature, heat flux density, ...)
    
```
<sequenceStep index="1">
    <analysis>
      <harmonic/>
      <transient/>
      <static/>
    </analysis>
    <pdeList> 
      <heatConduction>
        <regionList>
          <region name="S_wall"/>
        </regionList>
        <bcsAndLoads>
          <temperature name="L_left" value="25"/>
          <temperature name="L_right" value="-10"/>
        </bcsAndLoads>   
        <storeResults>
          <nodeResult type="heatTemperature">
            <allRegions/>
          </nodeResult>
          <elemResult type="heatFluxDensity">
            <allRegions/>
          </elemResult>
        </storeResults>
      </heatConduction>
    </pdeList>  
  </sequenceStep>
```

