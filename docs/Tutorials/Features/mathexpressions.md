# Math Expressions

The math expression parser allows to use simple mathematical expressions in combination with if statements inside of the xml files of openCFS. 

## Defining loads 
Math expressions can be used to define a synthetic load. For the following example a transient acoustic simulation (Singlefield -> AcousticPDE) is considered. The load is impressed onto a certain node, and defined as a sinus function 

```
 <bcsAndLoads>
    <pressure name="excite" value="2*sin(2*pi*100*t)"/>
 </bcsAndLoads>
```
In this example a sinus load with 100 Hz and an amplitude of 2 is defined. $t$ denotes the control variable. A different possibility for defining such a synthetic load is by using one of the custom functions of openCFS:

* spike(duration, timeval)
* fadeIn(duration, mode, timeval)
* sinBurst(freq, nperiods, nfadein, nfadeout, timeval)
* squareBurst(freq, nperiods, bipolar, pwidth, rtime, timeval)
* gauss(mue, sigma, normval, timeval)
* if(condition, trueval, elseval)

Following we will show two different examples for such custom functions: 

sinBurst :
```
 <bcsAndLoads>
    <pressure name="excite" value="2*sinBurst(4400,5,2,2,t)")"/>
 </bcsAndLoads>
```

if statement: 
```
 <bcsAndLoads>
    <pressure name="excite" value="(t lt 1)? (cos(2*pi*(t-0.5))+1)*sin(2*pi*t): 0")"/>
 </bcsAndLoads>
```


Furthermore, it is possible to superpose multiple different custom functions, with e.g. if statements. In the following example we superpose a time depending if statement with a spatial if statement
```
 <bcsAndLoads>
    <pressure name="exciteSurface" value="((t lt 1)? (cos(2*pi*(t-0.5))+1)*sin(2*pi*t): 0"))*((x lt 1)? (1 : 0))"/>
 </bcsAndLoads>
```

Hint: With multiple if statements it is especially important to look after the positioning of braces.


## Defining fields

Math expressions can also be used for defining e.g flow, or temperature fields. 
```
 <flowList>
  <flow name="backward_Z"> 
   <comp dof="x" value="0"/>
   <comp dof="y" value="120000/60*2*pi*z"/> 
   <comp dof="z" value="-120000/60*2*pi*y"/>
  </flow>
 </flowList>
```
This can also be used for defining blending functions, which e.g. necessary for the usage of non-conforming interfaces in combination with aeroacoustic source terms. For more details look into Singlefield -> AcousticPDE

## Usable functions

* exp(-1^t)
* sin(2*pi*100*t)
* cos(2*pi*100*t)
