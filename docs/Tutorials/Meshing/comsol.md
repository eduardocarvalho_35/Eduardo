# Using COMSOL Multiphysics as mesh-generator 

In order to use a mesh generated with COMSOL, the mesh has to be exported from COMSOl and converted to .hdf5 using "comsol_meshconvert.py". The following steps need to be taken: 

## Geometry 

If non-conforming grids are going to be used, it is important that the geometric domains are not merged via “union” to avoid collapsing the shared boundaries of the adjacent domains into one. Only the domains, whose shared interfaces are going to be conforming are merged by creating a “union”. 
At the end of the geometric sequence a “Form Assembly”-Node must be used, the “create pairs” box must be checked and the pairing type “identity pair” must be chosen. That way, the previously formed unions will stay unions and the rest of the domains will stay separate while the boundaries on both sides will be kept. 

![form_assembly.](form_assembly.JPG) 

## Selections 

All regions (domains, boundaries,..) need to be defined as “selections” and can later be used in openCFS by the name given in COMSOL. When the selections for the boundaries at non-conforming interfaces are created (master and slave), the boundary belonging to the correct region must be chosen. (boundaries of adjacent regions are indistinguishable, an easy way of selecting the boundary of one domain is by hiding the other domain). 

![selections](selections.JPG) 

# Mesh and export 

After the usual meshing-process the mesh is exported as .mphtxt via mesh -> export. It is important that the selections are included in the export. 

![export](export.JPG) 

## Conversion 

Change the filename in “comsol_meshconvert.py” accordingly and run the code (.mphtxt – file must be in the same folder).

