# Electric Flow PDE


## Governing Equations
The governing equation can easily be derived by considering Ampére's law and using Gauss' and Ohm's law in its stationary form (no time-derivative of the free charge density).
Mathematically this PDE is equivalent to the heat-conduction PDE, both of them are Poisson problems.


Introducing the electric potential $\mathbf{E} = -\nabla{\phi}$, we obatin 
\begin{equation}
-\nabla \cdot {\mathbf{\gamma}\nabla{\phi}} = 0,
\end{equation}
where $\gamma$ is the electric conductivity.
The boundary conditions can be given as
\begin{equation}
\phi = \phi_{\textrm e} \qquad \textrm{on }\Gamma_{\textrm e},
\end{equation}

\begin{equation}
\mathbf{J}\cdot\mathbf{n} = -\mathbf{\gamma}\nabla{\phi} \cdot\mathbf{n} = J_{\textrm n} \qquad \textrm{on }\Gamma_{\textrm n},
\end{equation}
with $\phi_{\textrm e}$ as the Dirichlet (essential) BC-value, $\Gamma_{\textrm e}$ the essential boundary, $\mathbf{n}$ the normal vector on Neumann (natural) boundary $\Gamma_{\textrm n}$ and $J_{\textrm n}$ the current density on the boundary.


## Boundary conditions
As described above, there are two types of BC's, Neumann and Dirichlet.
However, since openCFS is physics-based, there are several variations and combinations of these.
They can be defined in the simulation-xml file in the BC-section
```
<bcsAndLoads>
    <ground/>
    <normalCurrentDensity name="">
        <comp dof="x" value=""/>
        <comp dof="y" value=""/>
    </normalCurrentDensity>
    <potential name="" value=""/>
</bcsAndLoads>
```

* Ground `<potential name=""/>`: Homogeneous Dirichlet value $\phi_{\textrm{e}}=0$
* Normal Current Density `<normalCurrentDensity/>`: Defines normal component of current density $\mathbf{J}\cdot \mathbf{n}$ on a surface in the specified components `<comp dof="x" ... />`
* Potential `<potential name="" value=""/>`: Inhomogeneous Dirichlet value $\phi_{\textrm{e}}$

##Material
In general, the conductivity is a rank-2 tensor for anisotropic materials,
It can be specified in the material-xml file e.g. for copper either by defining an insotropic electric conductivity and/or a tensorial value for the anisotropic version
```
  <material name="polyethylene">
    <elecConduction>
      <electricConductivity>
	  <linear>
        <isotropic>
           <real> 5.8E7 </real>
        </isotropic>
        <tensor dim1="3" dim2="3">
          <real>
            5.8E7 0.0E+00 0.0E+00
            0.0E+00 5.8E7 0.0E+00
            0.0E+00 0.0E+00 5.8E7
          </real>
        </tensor>
	  </linear>
      </electricConductivity>
    </elecConduction>
  </material>
```


## Analysis Types
Since in general we are dealing with a time-independent PDE, we just have a static analysis:

* **Static:** 
```
<analysis>
  <static/>
</static>
```

## Postprocessing Results

#### Node Results
* **Electric Potential** (primary solution)
```
<nodeResult type="elecPotential">
```

#### Element Results
* **Electric Field Intensity** 
\begin{equation}
\mathbf E = -\nabla \phi
\end{equation}
```
<elemResult type="elecFieldIntensity">
```

* **Electric Current Density** 
\begin{equation}
J = \gamma \mathbf{E}
\end{equation}
```
<elemResult type="elecCurrentDensity">
```

* **Electric Power Density** 
\begin{equation}
e =  \gamma \mathbf{E}  \cdot \mathbf{E}
\end{equation}
```
<elemResult type="elecPowerDensity">
```


#### Surface Element Results
* **Electric Normal Current Density** 
\begin{equation}
J_{\textrm{n}} = J \cdot \mathbf n \cdot \mathbf n
\end{equation}
```
<surfElemeResult type="elecNormalCurrentDensity">
```


#### Surface Region Results
* **Electric Current** 
\begin{equation}
J = \int_{{A}}  J_{\textrm{n}}\, ds
\end{equation}
```
<surfRegionResult type="elecCurrent">
```


#### Region Results
* **Electric Power** 
\begin{equation}
E = \int_{{\Omega}} \gamma \mathbf{E}  \cdot \mathbf{E} \, dV
\end{equation}
```
<regionResult type="elecPower">
```



