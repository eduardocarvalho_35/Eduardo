# XML Editors for openCFS Input Files

The XML based simulation input files for openCFS can be written in any text editor.
However, specialised XML editors reading the XML-schema will greatly simplify input definition since they typically offer features like auto-completion and schema validation.

Below you find a view choices.

For an instruction on how to edit an xml file, see the [XML Turorial](../Tutorials/XMLExplanations/editing.md).
To find out more about the structure of the XML scheme, see the [XML Explantions](../Tutorials/XMLExplanations/structure.md) page.


## eclipse IDE (with xml plugin)

[eclipse IDE](https://www.eclipse.org/ide/) is a powerful IDE and supports XML auto-completion and shema validation.

First install eclipse, e.g. on Ubuntu Linux via the package manager
```shell
sudo apt install eclipse
```
Then open eclipse and go to `Help -> Eclipse Marketplace` and search for *Eclipse XML Editors* and install it.
Then restart eclipse.

![a1](xmleclipse.png)

### Activating XML-validation in eclipse
You can either open your xml files right away or you can use the full xml validation and suggestion abilities, which requires to create a 'dummy xml project' by right-clicking the project expolrer and create a new xml project, as shown in the screenshots below.

![a3](xml3.png)

![a2](xml2.png)

This creates a project, in which you can load the xml files.
The only things left to do is to restart eclipse and then open your xml file.


## Visual Studio Code

[Visual Studio Code](https://code.visualstudio.com/) has many plugins for XML files.

## oXygen

The commercial [oXygem XML Editor](https://www.oxygenxml.com/) supports auto-completion and schema validation (and much more).
It is available for all common platforms and they offer free trial versions.
