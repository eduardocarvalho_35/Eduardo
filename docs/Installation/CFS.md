# Installing openCFS

openCFS runs on Linux, macOS and Windows.
Below you find instructions for your operating system.

## Linux
We provide openCFS including all dependencies in a single archive.
The software should run on any recent linux system.

To install, just download the most recent archive, e.g. the recent master build: [CFS-master-Linux.tar.gz](https://opencfs.gitlab.io/cfs/CFS-master-Linux.tar.gz), and extract it to the desired location.

```shell
wget https://opencfs.gitlab.io/cfs/CFS-master-Linux.tar.gz
tar -xzvf CFS-master-Linux.tar.gz
```

This will extract to `CFS-<SHA>-Linux` where `<sha>` is the commit checksum of the installation.
The executables are found in the `bin` directory of the installation, e.g. in the directory `CFS-<SHA>-Linux/bin`.
You can add this directory to your `PATH` by running

```
export PATH=<absolute-cfs-installation-path>/bin:$PATH
```

To make this persistent add it to the configuration file of your shell, e.g. to `~/.bashrc` for bash.

You can now run cfs by
```shell
cfs -h
```

## macOS
We provide openCFS including a subsection of dependencies in a single archive.
For structural optimization you shall download the snopt and scip source from the respective code authors and compile openCFS yourself.

The provided binary is built with recent macOS (Intel) and not tested on former platforms.

For further help please follow the Linux instructions

Download a snapshot from March 2021 via this link [CFS-macOS_2021_03.tar.gz](https://faubox.rrze.uni-erlangen.de/getlink/fi7ctatGvtFGcAcSKQSKp1KN/CFS-macOS_2021_03.tar.gz)


## Windows

For Windows we currently provide a portable installation of openCFS in form of a zipped archive; a dedicated Windows installer is currently in preparation.

The build is based on a recent snapshot of the openCFS master, version 21.03, as of 03/23/2021. Intel Parallel Studio XE 2020 and Microsoft Visual Studio 2019 have been used for the compilation and the build has been performed on Windows 10, Version 20H2, Build 19042.867. Only a 64 bit version of openCFS is available for Windows. The portable version comes without an installation of the runtime libraries for Visual Studio 2019; the installer [VC_redist.x64.exe](https://aka.ms/vs/16/release/vc_redist.x64.exe) can be downloaded from Microsoft.

For the installation download the zipped archive [openCFS_21.03_win10_x64.zip](https://faubox.rrze.uni-erlangen.de/getlink/fiP7p6C5jFvRnrSKLLeWXEt3/openCFS_21.03_win10_x64.zip) and unzip this into any location that you prefer, let’s say `C:\Users\openCFS\21.03`. Next, define the environment variable `CFS_ROOT_DIR` pointing to the top directory of your openCFS installation. Using the above folder this will result in `CFS_ROOT_DIR=C:\Users\openCFS\21.03`.

To run openCFS, open a command shell, eventually define CFS_ROOT_DIR, if you have not already done so, which in our example results in 

```set CFS_ROOT_DIR=C:\Users\openCFS\21.03```

 and execute CFS by typing `%CFS_ROOT_DIR%\bin\cfs -h`. 
 
 To simplify the execution of openCFS you might consider adding `%CFS_ROOT_DIR%\bin` to your PATH environment variable by 

 ```set PATH=%CFS_ROOT_DIR%\bin;%PATH%```

### Windows - WSL
An alternative for running openCFS on Windows is the usage of [WSL](https://docs.microsoft.com/en-us/windows/wsl/). It lets one run a Linux environment on Windows without the overhead of a VM, or dual-boot setup. 
For the installation, please follow the provided guide [Manual-WSL-Installation](https://docs.microsoft.com/en-us/windows/wsl/install-win10#manual-installation-steps). It is recommended to use the Ubuntu distribution, since this is tested, but it should run on all distribution.   
After the installation of WSL and the distribution the Linux guide on installing openCFS can be followed.   
Please note that it could be necessary to change the permissions of the cfs folder. This can be done with ```sudo chmod -R 755 /cfs_dir```.   

It is possible that a shared library is missing from within the ubunut WSL (```libgomp.so.1```). It can be installed with ```sudo apt install libgomp1```

Afterwards it is possible to call openCFS from within powershell via ```wsl bash -c -i cfs --help```

To get GUI applications running in WSL, one can use [XLaunch](https://x.cygwin.com/docs/xlaunch/). For further details, there are different tutorials like [1](https://dev.to/egoist/running-linux-gui-programs-in-wsl2-29j3), [2](https://www.artemix.org/blog/linux-softwares-on-windows.html).
