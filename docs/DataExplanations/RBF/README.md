# Radial Basis Function

This interpolation filter interpolates field data from a source mesh to a target mesh based on radial basis functions (RBFs), where the local Wendland kernel together with a modified Shepard's method was chosen. Thereby, node-based data can be interpolated either to the cell centroids or the nodes of the source mesh, whereas element-based data can currently only be interpolated to the cell-centroids. The [theoretical background](https://doi.org/10.1002/nme.6298) of the implemented interpolation scheme is published in [@schoder2020c]. When using the conservative filters please provide a **citation** in you publication:

_Schoder, Stefan, et al. "Aeroacoustic source term computation based on radial basis functions."  International Journal for Numerical Methods in Engineering (2020)._


Like for all interpolation filters, the input and output quantitiy, the source and target regions, and the target mesh have to be defined within the xml-scheme. By using the optional tag *noSlipWall*, the output quantity of the nodes on the specified wall region (boundary surface) are set to zero. This tag is intended to consider no-slip-walls for the interpolation of the flow velocity but can theoretically also be used for other quantities. 

The field data is interpolated to the nodes of the target mesh  by default. By setting *useElemAsTarget* *true*, the field data is interpolated to the cell centroids of the target mesh. The tag *interpolationExponent* handles the locality of the approximation on the target mesh and is considered within the weight function (for details see [@schoder2020c]). The larger it is, the more local the approach making it less accurate but capable of resolving stronger gradients. Furthermore, *globalFactor* allows a scaling of the output by the defined factor.

The number of neighbours der target points $N_q$ und der influence points $N_w$ are set to $N_q=18$ and $N_w=13$, which is a compromise of accuracy and numerical efficiency for 2D and 3D interpolations. Thus, the tag *numNeighbours* is not considered in the current version of *openCFS*. By setting the tag *useCGAL4RBF* to *true*, the nearest neighbor search will be carried out be by [CGAL](https://www.cgal.org), which requires the *CGAL 3.3* build ([cmake flag must be set in the cmake build properties](../Installation/linuxinstall/)). 

```
<interpolation type="FieldInterpolation_RBF" id="interpolationRBCF" inputFilterIds="input"><scheme/>
	<scheme interpolationExponent="2" globalFactor="1.0E0" numNeighbours="13" useCGAL4RBF="false"/>
      	<useElemAsTarget>false</useElemAsTarget>
	<noSlipWall name="noSlipWall"/>
	<targetMesh>
		<hdf5 fileName="pathToMeshFile/meshFile.cfs"/>
	</targetMesh>
	<singleResult>
                <inputQuantity resultName="inputQuantity"/>
                <outputQuantity resultName="outputQuantity"/>
	</singleResult>
	<regions>
		<sourceRegions>
			<region name="sourceRegionR" />
		</sourceRegions>
		<targetRegions>
                    <region name="targetRegion"/>
                </targetRegions>
	</regions>
</interpolation>
```
## Acknowledgement
Please provide an **acknowledgement** at the end of your publication using this software part for simulations

_The computational results presented have been achieved [in part] using the software openCFS [Radial Basis Function]._

# References
\bibliography
